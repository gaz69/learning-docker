## Install docker, LinuxHotel method 

    apt-get install apt-transport-https ca-certificates curl software-properties-common
    curl -s https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt-get install docker-ce docker-ce-cli

    docker info



## Install docker from get.docker.com

Go to get.docker.com and follow download and install instructions in the top comments of the script itself.
It basically comes to this: 
    $ curl -fsSL https://get.docker.com -o get-docker.sh
    $ sh get-docker.sh
Note: 
  it was necessary to run:
  apt update 
    and
  apt upgrade
  in order to be able to run the script 

### Configure docker
    
    cat > /etc/docker/daemon.json <<EOF
    {
       "exec-opts": ["native.cgroupdriver=systemd"],
       "log-driver": "json-file",
       "log-opts": {
        "max-size": "100m"
    },
    "live-restore": true
    }
    EOF

    systemctl restart docker
    docker info


### Install docker-compose
    curl -L https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    chmod 755 /usr/local/bin/docker-compose

### Install docker-machine
can be found on github or at
https://docs.docker.com/machine/install-machine 
