Networking
==========

- By default the container use a **bridge** also called **docker0** network. All traffic going to the host is routed to an **edge firewall** which NATs the internal container IP-Address to the host IP-Address. This firewall (inside docker) blocks traffic on all ports except those specifically published with **-p** or **-P**. 
- Containers can "talk" to each other without having to publish ports with **-p**.
- Containers on the same **custom** network use an builtin DNS to resolve container names.
- Containers should not rely on IP-Addresses to communicate with each other. They should use the builtin DNS to locate each other. 
- Best practice is to create (new) virtual networks for containers that need to communicate with each other.

**Remember: docker defaults work well most of the time, but they are changeable if needed.**

docker network ls
=================
Displays all the networks that have been created

docker network inspect
======================
Displays information about a network

> docker network inspect [network]

docker network create 
=====================
Creates a network

> docker network create [options] [network_name]

options
-------
> --driver                                  : specifies the driver used to create the network 

docker network connect
======================
Connects an existing network to an existing container

> docker network connect [network] [container]

docker network disconnect
=========================
Disconnects a network from a container

> docker network disconnect [network] [container]
