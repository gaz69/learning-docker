# Dockerfile


Images are defined by a Dockerfile. A Dockerfile resembles a bash file but it follows a different syntax and rules.

A typical Dockerfile contains the following instructions:

## Instructions

FROM *base container* 
---------------------
The FROM instruction is required, it must be the first non-comment instruction in the file

RUN *shell commands* 
--------------------
The RUN instruction will be followed by shell commands. These commands will be executed by the default shell. It is preffered to group shell commands in a RUN instruction by chaining them with *&&* instead of creating a separated RUN instrucion for each.

WORKDIR *path*
--------------
The WORKDIR instruction sets the current directory in the container. This is preferred to using **cd [path]** within a RUN instruction. 

EXPOSE *port, port, ...*
-----------------------
With the EXPOSE instruction we define the list of ports that the container will expose to the outside. Remember that these ports must still be mapped to ports in the host when calling **docker container run** by using the **--port** or **-p** flags. 

ENV *VARIABLE=VALUE*
------------------
The ENV instruction creates environment variables for the container.

COPY *source path (host)* *target path (container)*
----
The COPY instruction allows to copy files from the host to the filessystem of the container. 

CMD
---
CMD adds default commands and or parameter for a container. CMD can be overriden by the external **docker container run** call.
It has three forms:

    CMD *shell command form*
    ------------------------
    Shell form of the CMD command

    CMD [*exec*,*param*,*param*]
    ----------------------------
    json form 

    CMD [*param*,*param*]
    ----------------------------
    as parameter for the ENTRYPOINT command

ENTRYPOINT
----------
The ENTRYPOINT instruction serves the same purpouse and has the same syntax as the CMD command. However it cannot be overriden by the **docker container run** unless the **--entrypoint** option is specified.

    ENTRYPOINT *shell command form*
    ------------------------
    Shell form of the instruction

    ENTRYPOINT [*exec*,*param*,*param*]
    ----------------------------
    json form of the instruction

## Other instructions

LABEL

MAINTAINER (deprecated)

ADD

VOLUME

USER

ARG

ONBUILD

STOPSIGNAL

HEALTHCHECK

SHELL


## Examples


### Dockerfile1
    FROM centos:latest
    RUN yum -y install httpd
    CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]
    VOLUME /var/www/html
    ADD index.html /var/www/html
    EXPOSE 80/tcp
### Dockerfile2 
    FROM     centos:latest
    ARG     ARG_WERT="arg_wert"
    ENV     ENV_WERT "env_wert"
    RUN     yum install -y httpd
    RUN     mkdir -p /usr/local/apache2/htdocs
    EXPOSE 80
    WORKDIR  "/usr/local/apache2/htdocs"
    RUN     echo $ARG_WERT > test.txt && \
             echo $ENV_WERT >> test.txt
    USER     ftp
    echo "test-as-ftp" > /tmp/test.txt
    USER     root
    CMD     [ "/usr/sbin/httpd", "-D", "FOREGROUND"]
### Dockerfile3
    FROM centos:latest
    CMD /usr/sbin/httpd -D FOREGROUND

    ENTRYPOINT ["htpasswd", "-c", "/usermanagement/.htpasswd"]
    CMD ["default"]

    RUN yum -y install httpd-tools httpd
    VOLUME /usermanagement

### Dockerfile4
    FROM centos:latest

    RUN yum install -y httpd wget ;\
    touch /var/www/html/index.html

    ONBUILD RUN echo "Image has changed!! CAREFUL!" > /var/www/html/index.html

    HEALTHCHECK --interval=15s --timeout=3s \
    CMD wget -q http://localhost || exit 1

    STOPSIGNAL 9

    LABEL "commit"="26dab679"
    LABEL "Version"="0.1.0-26dab679"

    CMD ["/usr/sbin/httpd","-D","FOREGROUND"]

### Dockerfile5:
    FROM centos:httpd
    CMD ["/usr/sbin/httpd","-D","FOREGROUND"]

    Dockerfile6
    FROM debian AS build
    RUN apt-get update && apt-get install -y fortune cowsay
    RUN /usr/games/fortune | /usr/games/cowsay > /tmp/test.txt


    FROM centos
    RUN yum -y  install httpd
    CMD ["/usr/sbin/httpd","-D","FOREGROUND"]
    COPY --from=build /tmp/test.txt /var/www/html/


    #FROM  centos AS deb.basic 
    ##FROM [--platform=<platform>] <image>[:<tag>] [AS <name>]
    # 
    #RUN apt-get update && apt-get install -y fortune cowsay
    #CMD ["/bin/bash", "-i" , "-l"]

    FROM [--platform=<platform>] <image>[:<tag>] [AS <name>]


