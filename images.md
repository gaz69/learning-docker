Images
======

- Contain the application binaries and dependecies as well as metadata about the image and how to run it.
- Images do not contain a kernel or kernel modules (like drivers). The kernel is provided by the host.
- The main or first source for images is https://hub.docker.com

Finding images in hub.docker.com
================================

- Image names follow the pattern _organisation/repository_ (example nginx/nginx-ingress, nginxdemos/hello).
- Official images are the only which do not include an organization in their name and are marked as **OFFICIAL IMAGE** on the hub.docker.com website.
- Official images are mantained by docker.inc. Official images are usually the best choice for start. They are usually very well documented.
- Image names are followed by their tag in the form _organisation/repository:tag_. Several tags can point to the same image.
- *alpine* based images are usually smaller and for that reason well suited for containers.

Layers
======
Images are made of layers. These layers are defined in the Dockerfile or through the *docker commit* command. Layers are stored only once and identified by their **sha**. When several images use the same layer, this layer is not stored for each of the images, instead the layer exists once in **docker cache** and shared by all the images that depend on it.

Copy on write (COW)
===================
Images are per definition read-only. Containers are read-write layer on top of the image itself. Live changes can be made to the files that comprise the image by changing this files in a running container. This changes are of course not persisted to the image. Instead, the changed file is copied to the container and used instead of the original image file. Thus a container *contains* the image files plus those files that are different in the container to the ones in the image. This is called **copy on write (COW)**.

docker image ls
===============
Shows images already downloaded and stored in the cache

> docker image ls

docker image history
====================
Shows the image layers that make up an image in the order in them they were added to the image. The newest changes are at the top.

> docker image history [image]


docker image inspect
====================
Shows image metadata. This includes things like:
- Ports
- Layers
- Environmental variables

> docker image inspect [image]