docker container run (alt: docker run)
======================================
The run command allows to run a container
> docker container run [options] container [command] [args..]

options
-------
> -it                                       : runs the container interactively.<br> 
> -d,--detach                               : runs the container in the background und returns to shell prompt.<br> 
> --rm                                      : remove the container as soon as the interactive session is terminated.<br >
> -p,--publish [hostport]:[containerport]   : maps containerport to host port<br> 
> -P,--publish-all                          : maps random hosts ports for all container ports<br> 
> --name containername                      : gives the container a name<br> 
> --mount                                   : a mount of type bind allows us to map a directory on the host to a directory in the container.<br>
 *type=bind,*<br> 
 *source=[directory_on_the_host],*<br> 
 *target=[directory_on_the_container]*<br>  
> --network [network]                       : run the container connected to a specific network (see networking)

docker container stop (alt: docker stop)
========================================
Stops a running container. The container can be started again by using docker start
> docker container stop [container]

docker container start (alt: docker start)
========================================
Starts a stopped or killed container. It requires that the container had been startet with a "docker run" before
> docker [options] container start [container]

options
-------
> -ai                                     : run interactively

docker container ls (alt: docker ps)
===================================
Shows container information. If called without options it will show only currently runing containers.
> docker container ls [options]

options
-------
> -a                                      : show all containers, also those not running

docker container top (alt: docker top)
====================
Shows running processes in a container. Analog to **top**.                                                  
> docker container top [container]      

docker container inspect (alt: docker inspect) 
==============================================
Shows information about how a container was started.
> docker container inspect [container]

docker container stats (alt: docker stats)
=========================================
Shows information about resources used by containers.
> docker container stats

docker container exec (alt: docker exec)
=========================================
Executes a command in a container that is already running

> docker [options] container [command] [args]

options
-------
> -it                                       : run the command interactively 

example
-------
*The following example expect that there is already a running container called nginx*<br> 
> docker exec -it nginx bash

docker container port (alt: docker port)
==========================================
Display port mapping (container-port --> host-port) for a specific container

> docker container port [container]



