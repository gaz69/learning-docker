## AUFGABE: Folgende Befehle im Container und im Hostsystem ausführen und vergleichen

    ps auxf | wc -l
    ps auxf
    ls /proc
    ip link list
    ip addr show
    df -h
    mount
    lsblk
    free -m
    uname -a
    hostname
    cat /etc/hosts

## what docker run -ti ubuntu /bin/bash really do
    docker pull 
    docker create
    docker start
    docker attach


docker ps
docker ps -a

docker start <container-id>
docker stop <container-id>


docker attach <container-id>
Wieder raus: Strg gedrückt halten und derweil P und Q nacheinander drücken

docker exec -ti <container-id> /bin/bash

docker run --restart=on-failure:2 -it ubuntu

docker run -d ubuntu /bin/bash -c "while true; do date; sleep 1; done"

docker run --log-driver=journald -d ubuntu /bin/bash -c "while true; do date; sleep 1; done"
docker logs -f <container-id>

docker inspect <container-id>

## AUFGABE: 
Webserver einrichten, anpassen und lokal testen. Wie erreicht man den Nachbarn?4. IP-Adresse des Containers rausfinden

1. Interaktiven Container starten
2. Apache Webserver installieren
3. Apache Webserver starten
4. IP-Adresse des Containers rausfinden
5. Startseite des Webservers im Browser auf dem Hostsystem abrufen

docker run -ti ubuntu /bin/bash
sten. Wie erreicht man den Nachbarn?

1. Interaktiven Container starten
2. Apache Webserver installieren
3. Apache Webserver starten
4. IP-Adresse des Containers rausfinden
$ apt-get update && apt-get install 
$ apt-get update && apt-get install -y apache2


apache2ctl start

docker inspect 2a --format '{{.NetworkSettings.IPAddress}}'

docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $INSTANCE_ID


### docker network

docker network create testnetz

docker network ls

docker run --network testnetz -ti ubuntu


## Aufgabe:

### Netzwerk erstellen
Zwei Container in dem neuen Netzwerk erstellen, ein Container wie gehabt erstellen

Fragen: Können sich die Container im selben Netz anpingen? Können sich die Container netzwerkübergrifend anpingen?

Nachvollziehen, welche Interfaces und Bridges es gibt (docker network inspect, ip addr show, iptables-save, ...)

docker run --network testnetz --name es1 --net-alias es elasticsearch:2

docker run --network testnetz --name es2 --net-alias es elasticsearch:2

host es1

host es2

host es

## Volume

docker volume

docker volume ls

docker volume create testvol

docker volume ls

docker volume inspect testvol

docker run --rm -ti -v testvol:/mnt ubuntu

https://db0smg.de/cgi-bin/liesel3.jpg


docker volume create htdocs

docker run -v htdocs:/mnt -d -it ubuntu /bin/bash -c "while true; do curl https://db0smg.de/cgi-bin/liesel3.jpg > /mnt/webcam.jpg; sleep 5; done"

docker run --rm -ti --volumes-from 3dec05ec4542 ubuntu

docker logs 3dec05ec4542AS

docker exec 3dec05ec4542 apt-get update

docker exec 3dec05ec4542 apt-get install curl -y

docker run -p 80:80 -v htdocs:/usr/local/apache2/htdocs -d httpd

https://docs.docker.com/engine/reference/builder

/usr/local/apache2/htdocs

env

echo $bla

echo $bla > /mnt/123







# docker-compose.yml

version: '2'

services:
  clock:
    image: clock:latest
  timeserver:
    image: httpd
  client:
    image: client:latest



docker-compose up

Was für Container, Netzwerke, Volumes hat docker-compose angelegt?

Wieso funktioniert die Kommunikation zwischen Client und Server (nicht)?

Was fehlt noch in der compose-Datei, damit die Uhrzeit erscheint?

https://docs.docker.com/compose/compose-file/

